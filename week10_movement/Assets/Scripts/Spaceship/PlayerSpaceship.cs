using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerDestroySound;
        [SerializeField] private float playerDestroySoundVolume = 0.1f;

        private AudioSource audioSource;
 
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(playerFireSound != null, "playerFireSound cannot be null");
            Debug.Assert(playerDestroySound != null, "playerDestroySound cannot be null");
            
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            AudioSource.PlayClipAtPoint(playerFireSound,Camera.main.transform.position,playerFireSoundVolume);
            
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioSource.PlayClipAtPoint(playerDestroySound,Camera.main.transform.position,playerDestroySoundVolume);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}