﻿using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController3 : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship3 enemySpaceship3;
        [SerializeField] private float chasingThresholdDistance;
        //[SerializeField] private PlayerSpaceship playerSpaceship;

        private PlayerSpaceship spawnedPlayerShip;
        
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship3.Fire();
        }

        private void MoveToPlayer()
        {
            // TODO: Implement this later

            spawnedPlayerShip = GameManager2.Instance.spawnedPlayerShip;
             
            if(spawnedPlayerShip == null)
                return;
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);

            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship3.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }
        }
    }    
}