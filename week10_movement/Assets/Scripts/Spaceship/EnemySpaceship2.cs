﻿using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship2 : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private double fireRate = 1;
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip enemyDestroySound;
        [SerializeField] private float enemyDestroySoundVolume = 0.1f;
        private float fireCounter = 0;
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioSource.PlayClipAtPoint(enemyDestroySound,Camera.main.transform.position,enemyDestroySoundVolume);
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            
            // TODO: Implement this later
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound,Camera.main.transform.position,enemyFireSoundVolume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}
