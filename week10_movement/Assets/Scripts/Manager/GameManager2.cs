﻿using System;
using Enemy;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager2 : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private EnemySpaceship2 enemySpaceship2;
        [SerializeField] private EnemySpaceship3 enemySpaceship3;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceship2Hp;
        [SerializeField] private int enemySpaceship3Hp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceship2MoveSpeed;
        [SerializeField] private int enemySpaceship3MoveSpeed;
        
        public static GameManager2 Instance { get; private set; }
        
        
        public PlayerSpaceship spawnedPlayerShip;
        
        
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(enemySpaceship2 != null, "enemySpaceship2 cannot be null");
            Debug.Assert(enemySpaceship3 != null, "enemySpaceship3 cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceship2Hp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceship3Hp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceship2MoveSpeed > 0, "enemySpaceship2MoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceship3MoveSpeed > 0, "enemySpaceship2MoveSpeed has to be more than zero");
            
            
            if (Instance == null)
            { 
                Instance = this;
            } 
            DontDestroyOnLoad(this);
            
            startButton.onClick.AddListener(OnStartButtonClicked);
          
            
            SoundManager.Instance.PlayBGM();
            
            
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }
        

        private void StartGame()
        {
          
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SpawnEnemySpaceship2();
            SpawnEnemySpaceship3();
        }
        
        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceship);
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;

            //var enemyController = spawnedEnemyShip.GetComponent<EnemyController>();
            //enemyController.Init(spawnedPlayerShip);
        }
        
        private void SpawnEnemySpaceship2()
        {
            var spawnedEnemyShip2 = Instantiate(enemySpaceship2);
            spawnedEnemyShip2.Init(enemySpaceship2Hp, enemySpaceship2MoveSpeed);
            spawnedEnemyShip2.OnExploded += OnEnemySpaceship2Exploded;

            //var enemyController2 = spawnedEnemyShip2.GetComponent<EnemyController2>();
            //enemyController2.Init(spawnedPlayerShip);
        }
        
        private void SpawnEnemySpaceship3()
        {
            var spawnedEnemyShip3 = Instantiate(enemySpaceship3);
            spawnedEnemyShip3.Init(enemySpaceship2Hp, enemySpaceship2MoveSpeed);
            spawnedEnemyShip3.OnExploded += OnEnemySpaceship3Exploded;

            //var enemyController3 = spawnedEnemyShip3.GetComponent<EnemyController3>();
            //enemyController3.Init(spawnedPlayerShip);
        }
        
        
        
        

        private void OnEnemySpaceshipExploded()
        {
            scoreManager.SetScore(1);
        }
        
        private void OnEnemySpaceship2Exploded()
        {
            scoreManager.SetScore(1);
        }
        
        private void OnEnemySpaceship3Exploded()
        {
            scoreManager.SetScore(1);
            SceneManager.LoadScene("Victory");
            Restart();
        }
        
        
        

        private void Restart()
        {
            DestroyRemainingShip();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShip()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }

    }
}
